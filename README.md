
# Métodos computacionales para ingeniería 1 - UNComa (2020)

## Datos de la práctica 3

Los datos se obtuvieron del siguiente dataset:

(Real and Fake Face Detection. Computational Intelligence and Photography Lab Department of Computer Science, Yonsei University)[https://www.kaggle.com/ciplab/real-and-fake-face-detection/data]